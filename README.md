#Grade Tracker Running Instructions:

The user should run complie the project and run GradeTracker > Views > MainDisplay.java
MainDisplay.java will handle all launching and running of the program.

#Main Menu:
* To add a course, click the '+' button in the bottom right
* Once a course has been added, click the pencil button next to the course name to edit it.
* Once a course has been added, click the x button next to the course name to delete it.

#Course Menu
* To see a course's information, click on the course ID, which is shadowed when the user hovers over it.
* To add a new assignment category, click the '+' button in the bottom right while in the course's menu
* Once an assignment category has been added, click the x button next to the assignment name to delete it.
* To edit a grade: Click the grade, enter a number, then press enter. If the grades are not editable, then the category is compound. To edit these grades, click the category name and enter the subfields.

#Adding new courses or assignments:
*  Add the required information into the fields as prompted, and press Enter or click Next as required.